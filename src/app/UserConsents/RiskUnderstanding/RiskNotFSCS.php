<?php
declare(strict_types=1);

namespace App\UserConsents\RiskUnderstanding;

/**
 * Class RiskNotFSCS
 *
 * @package App\UserConsents\RiskUnderstanding
 */
class RiskNotFSCS extends BaseRiskUnderstandingConsent
{
    /**
     * @var string
     */
    protected $key = 'risk_not_fscs';
}
