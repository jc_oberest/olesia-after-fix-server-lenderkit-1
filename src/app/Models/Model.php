<?php

namespace App\Models;

use LenderKit\Models\Model as LenderKitModel;

/**
 * Class Model
 *
 * @package App\Models
 */
abstract class Model extends LenderKitModel
{
    protected $guarded = ['id'];
}
