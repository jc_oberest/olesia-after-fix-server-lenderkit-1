<?php
/**
 * PHP Storm ssh client add extra \\ to class name when run tests. To prevent this we will manually remove them.
 */

// Check that we are running unit tests.
if (! defined('PHPUNIT_COMPOSER_INSTALL') || ! PHPUNIT_COMPOSER_INSTALL) {
    return;
}

if (! function_exists('phpstorm_fix_phpunit_args')) {
    /**
     * Phpstorm_fix_phpunit_args
     */
    function phpstorm_fix_phpunit_args()
    {
        foreach ($_SERVER['argv'] as $i => $arg) {
            if (false !== strpos($arg, 'Tests\\\\')
                && false === strpos($arg, '/(')
            ) {
                $_SERVER['argv'][$i] = str_replace('\\\\', '\\', $arg);
            }
        }
    }
}

phpstorm_fix_phpunit_args();
