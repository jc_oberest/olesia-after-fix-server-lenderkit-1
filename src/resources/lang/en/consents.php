<?php

return [
    'types' => [
        'risk_understanding' => 'Risk understanding',
    ],
    'risks' => [
        'titles' => [
            'risk_income_not_guarantee' => 'Income from investments is not guaranteed',
            'risk_difficult_to_sell' => 'Investments are long term in nature and it may be difficult to sell them before the end of the term',
            'risk_repayments' => 'Repayment of the investment depends on the sale of the property',
            'risk_unlisted_securities' => 'Investments should be diversified to unlisted securities of this nature',
            'risk_not_fscs' => 'The investments are not guaranteed by FSCS',
        ],
    ],
];
