<?php
/**
 * @copyright Copyright (c) JustCoded Ltd. All Rights Reserved.
 *    Unauthorized copying of this file, via any medium is strictly prohibited.
 *    Proprietary and confidential.
 *
 * @license https://lenderkit.com/license
 * @see https://lenderkit.com/
 *
 * @package LenderKit\Core
 */

return [
    'default' => env('SMS_CHANNEL', 'twilio'),
];
