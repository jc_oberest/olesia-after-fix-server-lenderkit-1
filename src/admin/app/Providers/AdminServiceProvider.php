<?php

namespace Admin\Providers;

use Admin\Services\Menu\SidebarMenuExtender;
use Illuminate\Support\AggregateServiceProvider;
use LenderKit\Admin\Services\Menu\SidebarMenu;

/**
 * Class AdminServiceProvider
 *
 * @package Admin\Providers
 */
class AdminServiceProvider extends AggregateServiceProvider
{
    /**
     * Providers
     *
     * @var array
     */
    protected $providers = [
        RouteServiceProvider::class,
        EventServiceProvider::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        parent::register();

        SidebarMenu::extend(SidebarMenuExtender::class);
    }
}
