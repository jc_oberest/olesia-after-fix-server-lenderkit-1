<?php

declare(strict_types=1);

namespace Api\Http\Resources;

use LenderKit\Api\Http\Resources\ResourceCollection as LenderKitResourceCollection;

/**
 * Class ResourceCollection
 *
 * @package Api\Http\Resources
 */
abstract class ResourceCollection extends LenderKitResourceCollection
{

}
