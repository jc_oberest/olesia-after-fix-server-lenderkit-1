<?php

declare(strict_types=1);

namespace Api\Http\Requests;

use LenderKit\Api\Http\Requests\Request as LenderKitApiRequest;

/**
 * Class Request
 *
 * @package Api\Http\Requests
 */
abstract class Request extends LenderKitApiRequest
{

}
