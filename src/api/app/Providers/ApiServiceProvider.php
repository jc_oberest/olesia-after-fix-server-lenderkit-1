<?php

namespace Api\Providers;

use Illuminate\Support\AggregateServiceProvider;
use LenderKit\Services\ApiDoc\Traits\LoadsApiDocs;

/**
 * Class ApiServiceProvider
 *
 * @package Api\Providers
 */
class ApiServiceProvider extends AggregateServiceProvider
{
    use LoadsApiDocs;
    /**
     * Providers
     *
     * @var array
     */
    protected $providers = [
        RouteServiceProvider::class,
        EventServiceProvider::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadApiDocsFrom(resource_path('swagger'), 'app');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        parent::register();
    }
}
