# Starting new project

This guide will help you to start a new project and configure it. You need to do several steps:

[TOC]

## Init repository

* Clone starter repository
* Remove .git folder: `rm -rf .git/`
* Connect your **project repository**
```bash
git init
git remote add origin {path/to/repository}
git add --all
git commit -m "Init project"
git push
```

## Files Cleanup

* Usually in commercial project you can't create users using Seeds, because they should be connected to a payment 
system. You need to update `src/database/seeds/DatabaseSeeder.php` and clean up what is not needed in your project
(for example, Themes Seeder).

* By default DatabaseSeeder has seeds to create admin user. You can also add admin with a command:    
```bash
php admin/artisan admin:create {email} {password} {name}
```

* Remove TestDataSeeder (and remove it from `src/Makefile` install target) if you don't need it.

* If you don't have some modules, you need to make sure they are not included in the admin assets inside
 `admin/resources/js/app.js`, and `admin/resources/sass/app.scss`. If so, you need to remove these lines.  
 Furthermore, you need to remove assets publishing inside `src/Makefile`, target `publish-admin-assets`.
    * Possible modules, which require assets cleanup:
        * Themes
        * RBAM
        * Investor Categorization
        * Rewards
        * GDPR
        * Slack

* Cleanup `src/.env.example` (and `src/.env` if you already init the project with `make init`) from variables you don't 
need (if you don't have these features in your project): payment providers variables, socialite, twilio, hellosign vars. 

* Cleanup `# DEV MODE ONLY!` part from `src/.gitignore` at the end of the file (line 42 and below). Usually you will 
update swagger, so you can't ignore it in the repo. For other admin assets it's okay to keep them ignored ONLY if you do 
not plan to update anything in admin styles/scripts. Otherwise, you must add all these folders to your git.

* Update **application level** Makefile (`src/Makefile`) `test` target to run `--testsuite Feature` instead of `Core`.

## Configure Environments

* Inside `/src/.env.example` and `/src/.env` variables `APP_NAME` with your project name
* If you have standard payment provider on your project - update `/src/.env.example` and `/src/.env` variables:  
    ```
    PAYMENT_PROVIDER={provider}
    KYC_PROVIDER={provider}
    ```
    (this can be done after installation)

* For **local development** you need to configure your project host and ports
    * Inside `/.env` variable `PHP_IDE_CONFIG` - update with your project name
    * If you have LK projects already on your machine after `make init` - change such variables:
        * Inside `/.env` variable update `HOST_*` to free ports
        * Inside `/docker-compose.yml` for service `web` update Host ports (left part) to free ports like  
        ```
        - {free port for api}:8888"
        - {free port for admin}:8443"
        ```
        * Inside `/src/.env` variables `APP_ADMIN_URL`, `APP_API_URL` update with new host and ports
    * Inside `nginx-server.conf` comment out `ssl*` directives, unless you have an SSL certificate for your
    domain

## Configure Service Providers

By default, the application connects all available modules available. You need to configure what you really need.

You need to do this for both Admin and API application:

* Edit `/src/admin/config/app.php` file. It loads almost whole config from lenderkit core, but you can overwrite 
specific sections if needed.
    * Leave only required payment provider
    * Remove Twilio and others if you don't have them in your project
    * LenderKit AdminServiceProvider is a must
    * App and Admin service providers are required as well
* Edit `/src/api/config/app.php` file. It loads almost whole config from lenderkit core, but you can overwrite 
specific sections if needed.
    * Leave only required payment provider
    * Remove Twilio and others if you don't have them in your project
    * LenderKit ApiServiceProvider is a must
    * App and Api service providers are required as well
    
## Update README.md

Update readme with project name, default host, URLs and admin user credentials info. 
